from flask import Flask, jsonify, request, redirect, url_for, make_response
from flask_restful import Api, Resource
from werkzeug.utils import secure_filename, send_from_directory
import os

app = Flask(__name__)
api = Api()
UPLOAD_FOLDER = ''
ALLOWED_EXTENSIONS = set(['png'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

# @app.route('/', methods=['GET', 'POST'])
class Main(Resource):
    def get(self):
        return '''
        <!doctype html>
        <title>Upload new File</title>
        <h1>Upload new File</h1>
        <form action="" method=post enctype=multipart/form-data>
        <p><input type=file name=file>
            <input type=submit value=Upload>
        </form>
        '''
    def post(self):
        if request.method == 'POST':
            file = request.files['file']
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                res = make_response('yes')
                return res
    # def upload_file():
    #     if request.method == 'POST':
    #         file = request.files['file']
    #         if file and allowed_file(file.filename):
    #             filename = secure_filename(file.filename)
    #             file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    #             return "yes"
    #     return '''
    #     <!doctype html>
    #     <title>Upload new File</title>
    #     <h1>Upload new File</h1>
    #     <form action="" method=post enctype=multipart/form-data>
    #     <p><input type=file name=file>
    #         <input type=submit value=Upload>
    #     </form>
    #     '''

api.add_resource(Main, "/")
api.init_app(app)

if __name__ == '__main__':
    app.run(debug=True)